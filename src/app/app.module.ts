import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import { AppComponent }  from './app.component';
import { NavbarComponent }  from './components/navbar.component';
import { ProfileComponent }  from './components/profile.component';

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule ],
  declarations: [ AppComponent, ProfileComponent,NavbarComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }

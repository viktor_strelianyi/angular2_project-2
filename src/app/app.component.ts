import { Component } from '@angular/core';
import {GitHubService} from './services/github.service';

@Component({
  selector: 'my-app',
  template: ` 
  <navbar></navbar>
  <div class="container">
    <profile></profile>
  </div>  
  `,
  providers: [GitHubService]
})
export class AppComponent  { }

import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GitHubService {
  private username: string;
  private client_id = '26e994692e013815dc89';
  private client_secret = '28a6a55abe0585fdc9aa48c94c3107289a6c0b4b';

  constructor( private _http: Http){
    console.log('GitHub service ready...');
    this.username = 'vstrelianyi';
  }

  getUser(){
    return this._http.get('http://api.github.com/users/'+ this.username + '?client_id=' +this.client_id +'&client_secret='+ this.client_secret)
      .map(res => res.json());
  }

  getRepos(){
    return this._http.get('http://api.github.com/users/'+ this.username + '/repos?client_id=' +this.client_id +'&client_secret='+ this.client_secret)
      .map(res => res.json());
  }

  updateUser(username:string){
    this.username = username;
  }
}